package by.test.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by homeUser on 1/23/2016.
 */
@SpringBootApplication
@ComponentScan(basePackages = "by.test")
public class AppInit {
    public static void main(String[] args) {
        SpringApplication.run(AppInit.class, args);
    }
}
