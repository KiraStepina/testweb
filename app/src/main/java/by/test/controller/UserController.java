package by.test.controller;

import by.test.dm.UserEntity;
import by.test.repository.UserRepository;
import by.test.dto.UserFetchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by homeUser on 1/23/2016.
 */
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void create(@RequestBody UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    @RequestMapping(value = "/read", method = RequestMethod.POST)
    public List<UserEntity> read(@RequestBody UserFetchDto userFetchDto) {
        PageRequest pageRequest = new PageRequest(userFetchDto.getPage() - 1, userFetchDto.getPageSize(), userFetchDto.getDirection(), userFetchDto.getFieldName());
        Page<UserEntity> page = userRepository.findAll(pageRequest);
        return page.getContent();
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public UserEntity update(@RequestBody UserEntity userEntity) {
        userRepository.save(userEntity);
        return userEntity;
    }

    @RequestMapping(value = "/delete/{userId}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long userId) {
        userRepository.delete(userId);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public long getCount(){
        return userRepository.count();
    }
}
