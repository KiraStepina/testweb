package by.test.dto;

import org.springframework.data.domain.Sort;

/**
 * Created by homeUser on 1/23/2016.
 */
public class UserFetchDto {
    private int page;
    private int pageSize;
    private String fieldName;
    private Sort.Direction direction;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Sort.Direction getDirection() {
        return direction;
    }

    public void setDirection(Sort.Direction direction) {
        this.direction = direction;
    }
}
