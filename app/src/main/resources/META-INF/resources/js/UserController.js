angular.module('app').controller('UserController', ['$scope', '$http', '$mdDialog', function ($scope, $http, $mdDialog) {
    'use strict';

    $scope.usersList = [];
    $scope.selected = [];
    $scope.sortDirection = {};
    $scope.userCount;

    $scope.query = {
        page: 1,
        pageSize: 5,
        fieldName: "id",
        direction: "ASC"
    };

    function getUsersList(query) {
        $http.post("users/read",
            query).success(function (users) {
                $scope.usersList = users;
                $scope.getUserCount();
            });
    }

    function deleteUser(user) {
        $http.delete("users/delete/" + user.id).success(function () {
            $scope.query.page = 1;
            getUsersList($scope.query);
        });
    }

    function success(userList) {
        $scope.usersList = userList;
    }

    $scope.onPaginate = function (page, limit) {
        getUsersList(angular.extend({}, $scope.query, {page: page, pageSize: limit}));
    };

    $scope.onReorder = function (order) {
        $scope.query.direction = "ASC";
        $scope.query.fieldName = order;
        if (order[0] == "-") {
            $scope.query.direction = "DESC";
            $scope.query.fieldName = order.substring(1);
        }
        getUsersList($scope.query);
    };

    $scope.addNewUser = function (ev) {
        var useFullScreen = false;
        $mdDialog.show({
            controller: 'UserDialogController',
            templateUrl: 'html/user-dialog.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {currentUser: null}
        })
            .then(function (answer) {
                getUsersList($scope.query);
            });
    };

    $scope.updateUser = function (user) {
        var userCopy = angular.extend({}, user);
        var useFullScreen = false;
        $mdDialog.show({
            controller: 'UserDialogController',
            templateUrl: 'html/user-dialog.html',
            parent: angular.element(document.body),
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {currentUser: userCopy}
        })
            .then(function (answer) {
                getUsersList($scope.query);
            });
    };

    $scope.deleteUser = function (user) {
        var confirm = $mdDialog.confirm()
            .title('Would you like to delete this user?')
 //           .textContent('All of the banks have agreed to forgive you your debts.')
            .ariaLabel('Lucky day')
            .ok('Delete')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function () {
            deleteUser(user);
        });
    };

    $scope.getUserCount = function() {
        $http.get("users/count").success(function (data) {
                $scope.userCount = data;
            }
        );
    };

    getUsersList($scope.query);
}]);