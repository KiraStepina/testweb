angular.module('app').controller('UserDialogController', ['$scope', '$http','$mdDialog', 'currentUser', function ($scope, $http, $mdDialog, currentUser) {
    'use strict';
    $scope.user = currentUser || {};
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.addNewUser = function () {
        if ($scope.user){
            $http.post("users/create",
                $scope.user).success(function () {
                    $scope.hide();
                });
        }
        else {
            $http.put("users/update",
                $scope.user).success(function () {
                    $scope.hide();
                });
        }
    };
}]);