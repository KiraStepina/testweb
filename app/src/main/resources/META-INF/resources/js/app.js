var app = angular.module('app', ['ngAnimate','ngAria', 'ngMaterial', 'md.data.table']);

app.factory('ajaxRequestInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    return {
        'request': function (config) {
            config.silent || ($rootScope.ajaxConnections ?
                $rootScope.ajaxConnections++ : $rootScope.ajaxConnections = 1);
            return config || $q.when(config);
        },
        'requestError': function (rejection) {
            rejection.config.silent || $rootScope.ajaxConnections--;
            return $q.reject(rejection);
        },
        'response': function (response) {
            response.config.silent || $rootScope.ajaxConnections--;
            return response || $q.when(response);
        },
        'responseError': function (rejection) {
            rejection.config.silent || $rootScope.ajaxConnections--;
            console.error("Unexpected system error: " + rejection.status);

            return $q.reject(rejection);
        }
    }
}]);
