package by.test.controller;

import by.test.config.AppInit;
import by.test.dm.UserEntity;
import by.test.repository.UserRepository;
import by.test.dto.UserFetchDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by homeUser on 1/27/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(AppInit.class)
@WebIntegrationTest
public class UserControllerTest {

    @Autowired
    private UserRepository userRepository;

    RestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testUserCreation() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("testUserName");
        userEntity.setFirstName("testFirstName");
        userEntity.setLastName("testLastName");
        restTemplate.postForEntity("http://localhost:8081/users/create", userEntity, Void.class);
        List<UserEntity> foundUserEntityList = userRepository.findAll();
        Assert.assertTrue(foundUserEntityList.stream().anyMatch(user -> user.getUserName().equals(userEntity.getUserName())
                && user.getFirstName().equals(userEntity.getFirstName()) && user.getLastName().equals(userEntity.getLastName())));
    }

    @Test
    public void testUserLookUp() throws URISyntaxException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("testUserName1");
        userEntity.setFirstName("testFirstName1");
        userEntity.setLastName("testLastName1");
        UserEntity resultUserEntity = userRepository.save(userEntity);
        UserFetchDto userFetchDto = new UserFetchDto();
        userFetchDto.setPage(1);
        userFetchDto.setPageSize(Integer.MAX_VALUE);
        userFetchDto.setFieldName("id");
        userFetchDto.setDirection(Sort.Direction.ASC);
        RequestEntity<UserFetchDto> requestEntity = new RequestEntity<>(userFetchDto, HttpMethod.POST, new URI("http://localhost:8081/users/read"));
        List<UserEntity> resultUserList = restTemplate.exchange(requestEntity, new ParameterizedTypeReference<List<UserEntity>>() {
        }).getBody();
        Assert.assertTrue(resultUserList.stream().anyMatch(user -> user.getId().equals(resultUserEntity.getId())));
    }

    @Test
    public void testUserUpdate() throws URISyntaxException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("testUserName2");
        userEntity.setFirstName("testFirstName2");
        userEntity.setLastName("testLastName2");
        UserEntity resultUserEntity = userRepository.save(userEntity);
        resultUserEntity.setUserName("testUserName21");
        restTemplate.put(new URI("http://localhost:8081/users/update"), resultUserEntity);
        Assert.assertEquals(userRepository.findOne(resultUserEntity.getId()).getUserName(), "testUserName21");
    }

    @Test
    public void testUserDelete() throws URISyntaxException {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName("testUserName3");
        userEntity.setFirstName("testFirstName3");
        userEntity.setLastName("testLastName3");
        UserEntity resultUserEntity = userRepository.save(userEntity);
        restTemplate.delete(new URI("http://localhost:8081/users/delete/"+resultUserEntity.getId()));
        Assert.assertNull(userRepository.findOne(resultUserEntity.getId()));
    }

}