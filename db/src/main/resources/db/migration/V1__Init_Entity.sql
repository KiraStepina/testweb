CREATE TABLE users (
  id       BIGINT       NOT NULL AUTO_INCREMENT,
  user_name VARCHAR(255) NOT NULL,
  first_name VARCHAR(255) NOT NULL,
  last_name     VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
INSERT INTO users (id, user_name, first_name, last_name) VALUES (1, 'user_name1', 'first_name1', 'last_name1');
INSERT INTO users (id, user_name, first_name, last_name) VALUES (2, 'user_name2', 'first_name2', 'last_name2');
INSERT INTO users (id, user_name, first_name, last_name) VALUES (3, 'user_name3', 'first_name3', 'last_name3');
INSERT INTO users (id, user_name, first_name, last_name) VALUES (4, 'user_name4', 'first_name4', 'last_name4');
INSERT INTO users (id, user_name, first_name, last_name) VALUES (5, 'user_name5', 'first_name5', 'last_name5');
INSERT INTO users (id, user_name, first_name, last_name) VALUES (6, 'user_name6', 'first_name6', 'last_name6');