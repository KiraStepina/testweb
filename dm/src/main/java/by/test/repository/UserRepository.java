package by.test.repository;

import by.test.dm.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by homeUser on 1/24/2016.
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {

}
